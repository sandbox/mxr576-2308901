<?php

/**
 * @file
 * Administrative interface for Open Graph meta info importer settings.
 */

/**
 * Form constructor for the 'OG meta info importer settings' form of a bundle.
 *
 * @see open_graph_meta_info_importer_settings_form_validate()
 * @see open_graph_meta_info_importer_settings_form_submit()
 */
function open_graph_meta_info_importer_settings_form($form, &$form_state, $entity_type, $bundle) {
  $bundle = field_extract_bundle($entity_type, $bundle);

  // When displaying the form, make sure the list of fields is up-to-date.
  if (empty($form_state['post'])) {
    field_info_cache_clear();
  }

  // Gather bundle information.
  $instances = field_info_instances($entity_type, $bundle);
  $extra_fields = field_info_extra_fields($entity_type, $bundle, 'form');

  $form += array(
    '#entity_type' => $entity_type,
    '#bundle' => $bundle,
    '#fields' => array_keys($instances),
    '#instances' => $instances,
    '#extra_fields' => $extra_fields,
  );

  $og_tag_associations = open_graph_meta_info_importer_get_og_tag_assignments('all');
  $og_tags = array_keys($og_tag_associations);

  $form['open_graph_meta_info_importer_assignments_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Open Graph mate info importer'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#description' => t('Set up Open Graph meta info assignments for existing fields.'),
  );

  foreach ($og_tags as $og_tag) {
    $form['open_graph_meta_info_importer_assignments_fieldset']['og_tags'][$og_tag] = array(
      '#title' => $og_tag,
      '#type' => 'select',
      '#options' => open_graph_meta_info_importer_get_assignable_field_instances($og_tag, $instances, $extra_fields),
      '#default_value' => open_graph_meta_info_importer_get_assigned_field($og_tag, $bundle, $instances),
    );
  }

  $form['open_graph_meta_info_importer_assignments_fieldset']['markup'] = array(
    '#markup' => t('Each field should be assigned only one Open Graph tag. Og:image and og:image:secure_url cannot be used in multivalued file or image fields!'),
  );

  $form['open_graph_meta_info_importer_assignments_fieldset']['actions'] = array('#type' => 'actions');
  $form['open_graph_meta_info_importer_assignments_fieldset']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Assign'),
  );

  return $form;
}

/**
 * Form validation handler for open_graph_meta_info_importer_settings_form().
 *
 * @see open_graph_meta_info_importer_settings_form_submit()
 */
function open_graph_meta_info_importer_settings_form_validate($form, &$form_state) {
  $assigned_instances = array();
  foreach (array_keys($form_state['values']['open_graph_meta_info_importer_assignments_fieldset']['og_tags']) as $og_tag) {
    $field = $form_state['values']['open_graph_meta_info_importer_assignments_fieldset']['og_tags'][$og_tag];
    if ($field != '_none' && in_array($field, $assigned_instances)) {
      form_set_error('open_graph_meta_info_importer_assignments_fieldset][og_tags][' . $og_tag, t('Selected field already assigned to an other Open Graph tag.'));
    }
    else {
      $assigned_instances[] = $form_state['values']['open_graph_meta_info_importer_assignments_fieldset']['og_tags'][$og_tag];
    }
  }
}

/**
 * Form submission handler for open_graph_meta_info_importer_settings_form().
 *
 * @see open_graph_meta_info_importer_settings_form_validate()
 */
function open_graph_meta_info_importer_settings_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['values']['open_graph_meta_info_importer_assignments_fieldset']['og_tags']) as $og_tag) {
    $prev_assigned = open_graph_meta_info_importer_get_assigned_field($og_tag, $form['#bundle'], $form['#instances']);
    if (in_array($prev_assigned, $form['#fields'])) {
      unset($form['#instances'][$prev_assigned]['open_graph_meta_info_importer']);
      field_update_instance($form['#instances'][$prev_assigned]);
    }
    else {
      variable_del('open_graph_meta_info_importer_' . $form['#bundle'] . '_' . $og_tag);
    }
    $assigned_instance = $form_state['values']['open_graph_meta_info_importer_assignments_fieldset']['og_tags'][$og_tag];
    if (in_array($assigned_instance, $form['#fields'])) {
      $form['#instances'][$assigned_instance]['open_graph_meta_info_importer']['assigned_tag'] = $og_tag;
      field_update_instance($form['#instances'][$assigned_instance]);
    }
    else {
      variable_set('open_graph_meta_info_importer_' . $form['#bundle'] . '_' . $og_tag, $assigned_instance);
    }
  }
}
