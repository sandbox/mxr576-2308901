CONTENTS OF THIS FILE
---------------------
* Introduction
* Installation
* Configuration


INTRODUCTION
------------
If you enable Open Graph meta information importer module then you will able to
import Open Graph meta information into your content on node add/edit page
easily.

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
* Enable module for content type on admin/structure/types/manage/<content type>.
* Now you will able to assign existing fields to Open Graph meta tags in
  admin/structure/types/manage/<content type>/og-meta-info-importer page.
* After that you will have only one thing to do: go to node/add/<content type>
  page and use the importer.
